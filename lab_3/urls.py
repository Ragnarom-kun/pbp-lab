from django.urls import path, re_path
from .views import index, add_friend

urlpatterns = [
    path('', index, name='index'),
    # Adding path for add_friend
    path('add', add_friend, name='add_friend'),
]
