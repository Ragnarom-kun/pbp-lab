from .models import Friend
from django.forms import ModelForm

# from GeeksforGeeks
class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields="__all__"
