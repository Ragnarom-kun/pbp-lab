from django.db import models
from datetime import date

# Creating my models

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    DOB = models.DateField(default=date.today)