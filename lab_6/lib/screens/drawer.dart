import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            currentAccountPicture: new CircleAvatar(
                radius: (52),
                backgroundColor: Colors.white,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Image.asset("assets/images/profile.jpg"),
                )),
            accountName: Text("Romi"),
            accountEmail: Text("romi.donor@kovalen.com"),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.accessibility_new,
            title: "About Us",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.featured_play_list_rounded,
            title: "Features",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.feedback_outlined,
            title: "Feedback",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.contact_mail_outlined,
            title: "Contact Us",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.logout,
            title: "Log out",
            onTilePressed: () {},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;
  const DrawerListTile(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onTilePressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
