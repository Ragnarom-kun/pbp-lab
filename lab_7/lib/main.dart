import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lab_7/body/report.dart';
import 'package:lab_7/screens/drawer.dart';

void main() => runApp(new MainApp());

class MainApp extends StatefulWidget {
  DashboardDonor createState() => DashboardDonor();
}

class DashboardDonor extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "KonvaSearch",
        theme: ThemeData(
          // Define the colors.
          primarySwatch: Colors.indigo,

          // Define the default font family.
          fontFamily: 'ShipporiAntiqueB1-Regular',
        ),
        home: MyHome());
  }
}

class MyHome extends StatefulWidget {
  @override
  _MyHomeState createState() => _MyHomeState();
}

int _selectedNavbar = 0;

class _MyHomeState extends State<MyHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(children: [
            Container(
                child: Image.asset(
                  "assets/images/logo.png",
                  width: 36.0,
                  height: 36.0,
                ),
                margin: const EdgeInsets.fromLTRB(0, 0, 6.0, 0)),
            Text("KonvaSearch",
                style: const TextStyle(fontWeight: FontWeight.w600)),
          ]),
          actions: [
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(
                Icons.more_vert,
              ),
              onPressed: () {},
            )
          ],
          backgroundColor: Colors.indigo,
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.grey[350],
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home_rounded), label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.dashboard_rounded,
                ),
                label: 'Dashboard'),
            BottomNavigationBarItem(
                icon: Icon(Icons.question_answer_rounded), label: 'FaQ')
          ],
          currentIndex: _selectedNavbar,
          selectedItemColor: Colors.red.shade600,
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          onTap: (index) {
            setState(() {
              _selectedNavbar = index;
            });
          },
        ),
        drawer: DrawerScreen(),
        body: ListView(
          children: <Widget>[
            const ListTile(
              title: Text(
                "Halo, Romi.",
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'ShipporiAntiqueB1-Regular'),
              ),
              subtitle: Text(
                "Anda berstatus sebagai pendonor.",
                style: TextStyle(fontSize: 17),
              ),
            ),
            ListTile(
              leading: Icon(Icons.bloodtype),
              title: Text("Requests"),
              subtitle: Text("Membuat request sebagai calon pendonor."),
              onTap: () {
                debugPrint("Request pressed");
              },
            ),
            ListTile(
              leading: Icon(Icons.report_problem_rounded),
              title: Text("Report"),
              subtitle: Text("Masukkan perihal yang ingin disampaikan."),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ReportPage()),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.notifications_active),
              title: Text("Notifications"),
              subtitle: Text("Cek notifikasi request donor darah."),
              onTap: () {
                debugPrint("Notifications pressed");
              },
            ),
          ],
        ));
  }
}
