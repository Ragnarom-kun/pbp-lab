from django.forms import ModelForm
from .models import Note

# from GeeksforGeeks
class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields="__all__"