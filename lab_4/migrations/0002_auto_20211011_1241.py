# Generated by Django 3.2.7 on 2021-10-11 05:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='message',
            field=models.CharField(max_length=800),
        ),
        migrations.AlterField(
            model_name='note',
            name='title',
            field=models.CharField(max_length=50),
        ),
    ]
