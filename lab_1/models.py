from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here
from django.utils import timezone
from datetime import datetime, date


class Friend(models.Model):
    name = models.CharField(max_length=30)
    # TODO Implement missing attributes in Friend model
    npm = models.BigIntegerField()
    DOB = models.DateField(default=date.today)