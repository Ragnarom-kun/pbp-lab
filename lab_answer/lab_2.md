Romi Fadhurrohman Nabil
2006535016

1. Apakah perbedaan antara JSON dan XML?
    - XML adalah markup language sehingga ada tag di tiap elemen, sedangkan JSON adalah format yang ditulis dalam JavaScript
    - Data XML disimpan sebagai tree structure, sedangkan JSON disimpan seperti map yaitu terdapat key dan value
    - Data types yang didukung XML bervariasi seperti gambar, teks, grafik; sedangkan JSON berupa teks dan angka
    - XML mendukung namespaces, komentar dan metadata; sedangkan JSON tidak memiliki ketentuan untuk namespace, menambahkan komentar atau menulis metadata
    - XML tidak mendukung array secara langsung, sedangkan JSON mendukung array yang dapat diakses

2. Apakah perbedaan antara HTML dan XML?
    - HTML berfokus pada penyajian data, sedangkan XML berfokus pada transfer data
    - HTML case insensitive, sedangkan XML case sensitive
    - HTML tidak mendukung namespaces, sedangkan XML mendukung namespaces
    - HTML tidak strict untuk tag penutup, sedangkan XML strict untuk tag penutup
    - HTML memiliki tag terbatas, sedangkan XML tagnya dapat dikembangkan


Referensi :
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://blogs.masterweb.com/perbedaan-xml-dan-html/
