from django.db import models


class Note(models.Model):
    to = models.CharField(max_length=50)
    from_who = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    message = models.CharField(max_length=400)
